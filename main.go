package main

import (
	"fmt"
	"log"
	"os"
	"os/signal"
	"strconv"
	"strings"
	"syscall"

	"github.com/bwmarrin/discordgo"
	"github.com/joho/godotenv"
	"gitlab.com/VFay/cossacks-game-randomizer/services"
	"gitlab.com/VFay/cossacks-game-randomizer/utils"
)

func main() {
	if err := godotenv.Load(); err != nil {
		log.Fatal("Error loading .env file")
	}

	var service services.SetupService
	service.Init()

	token := os.Getenv(utils.TokenKey)
	session, err := discordgo.New("Bot " + token)

	if err != nil {
		log.Fatal(err)
	}

	session.AddHandler(func(s *discordgo.Session, m *discordgo.MessageCreate) {
		if m.Author.ID == s.State.User.ID {
			return
		}

		args := strings.Split(m.Content, " ")

		if len(args) == 0 || args[0] != utils.BotPrefix {
			return
		}

		numberOfPlayers, err := strconv.Atoi(args[1])

		if err != nil || numberOfPlayers > utils.MaxNumberOfPlayers {
			return
		}

		gameSetup := service.GenerateGameSetup(numberOfPlayers)

		s.ChannelMessageSend(m.ChannelID, gameSetup)
	})

	session.Identify.Intents = discordgo.IntentsAllWithoutPrivileged

	err = session.Open()

	if err != nil {
		log.Fatal(err)
	}
	defer session.Close()

	fmt.Println("bot online")

	sc := make(chan os.Signal, 1)
	signal.Notify(sc, syscall.SIGINT, syscall.SIGTERM, os.Interrupt)
	<-sc
}
